package com.example.login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Second extends AppCompatActivity {
    TextView info;
    Intent nIntent;
    String login;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        info =(TextView) findViewById(R.id.secondText);
        nIntent = getIntent();//конструктор + .var

        login = nIntent.getStringExtra(MainActivity.FLAG);

        info.setText(getString(R.string.secondTitle) +", "+ login);

    }
}
