package com.example.login;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static final String FLAG = "flag";
    TextView title;
    Intent intent;
    EditText loginText;
    EditText passText;
    String login;
    String pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button enter = (Button) findViewById(R.id.enter);

        title = (TextView) findViewById(R.id.title);
        intent = new Intent(this, Second.class);

        loginText = (EditText) findViewById(R.id.login);
        passText = (EditText) findViewById(R.id.pass);


        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login = loginText.getText().toString();
                pass = passText.getText().toString();
                if("".equals(login)) loginText.setText("");
                if (!"123".equals(pass)) {
                        title.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));
                } else {
                    title.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
                    intent.putExtra(FLAG, login);
                    startActivityForResult(intent,1);
                    //startActivity(intent);
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
